using IdolGroup.Models; 
using Microsoft.EntityFrameworkCore; 
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;

namespace IdolGroup.Data 
{ 
    public class IdolGroupContext : IdentityDbContext<NewsUser>
    { 
        public DbSet<Jobs> newsList { get; set; } 
        public DbSet<JobsCategory> NewsCategory { get; set; } 
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
         { 
             optionsBuilder.UseSqlite(@"Data source=IdolGroup.db"); 
        } 
    } 
}