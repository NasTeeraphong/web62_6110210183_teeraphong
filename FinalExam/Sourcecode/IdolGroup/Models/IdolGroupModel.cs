using System; 
using System.ComponentModel.DataAnnotations; 
using Microsoft.AspNetCore.Identity;

namespace IdolGroup.Models 
{ 
    public class NewsUser : IdentityUser
    { 
    public string FirstName { get; set;} 
    public string LastName { get; set;} 
    }

    public class JobsCategory 
    { 
        public int JobsCategoryID { get; set;} 
        public string ShortName {get; set;}
        public string FullName { get; set;} 
    }
    public class Jobs
    { 
        public int JobsID { get; set; } 

        public int JobsCategoryID { get; set; } 
        public JobsCategory NewsCat { get; set; } 

        [DataType(DataType.Date)] 
        public string ReportDate { get; set; } 
        public string JobsDetail { get; set; } 
        public float GpsLat { get; set; } 
        public float GpsLng { get; set; } 

        public string NewsUserId {get; set;} 
        public NewsUser postUser {get; set;}
    } 
}