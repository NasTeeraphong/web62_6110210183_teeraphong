﻿using System; 
using System.Collections.Generic; 
using System.Linq; 
using System.Threading.Tasks; 
using Microsoft.AspNetCore.Mvc; 
using Microsoft.AspNetCore.Mvc.RazorPages; 

using Microsoft.EntityFrameworkCore; 
using IdolGroup.Data;
using IdolGroup.Models;

namespace IdolGroup.Pages
{
    public class IndexModel : PageModel
    {
        private readonly IdolGroup.Data.IdolGroupContext _context;

        public IndexModel(IdolGroup.Data.IdolGroupContext context)
        {
            _context = context;
        }

        public IList<Jobs> Jobs { get;set; }
        public IList<JobsCategory> NewsCategory { get;set; }

        public async Task OnGetAsync()
        {
            Jobs = await _context.newsList
                .Include(j => j.NewsCat).ToListAsync();
                NewsCategory = await _context.NewsCategory.ToListAsync();
        }
    }
}
