using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using IdolGroup.Data;
using IdolGroup.Models;

namespace IdolGroup.Pages.NewsCategoryAdmin
{
    public class CreateModel : PageModel
    {
        private readonly IdolGroup.Data.IdolGroupContext _context;

        public CreateModel(IdolGroup.Data.IdolGroupContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["JobsCategoryID"] = new SelectList(_context.NewsCategory, "JobsCategoryID", "ShortName"); 
        return Page();
        }

        [BindProperty]
        public Jobs Jobs { get; set; }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.newsList.Add(Jobs);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}