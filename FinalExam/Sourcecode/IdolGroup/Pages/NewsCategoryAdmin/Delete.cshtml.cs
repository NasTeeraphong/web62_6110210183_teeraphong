using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using IdolGroup.Data;
using IdolGroup.Models;

namespace IdolGroup.Pages.NewsCategoryAdmin
{
    public class DeleteModel : PageModel
    {
        private readonly IdolGroup.Data.IdolGroupContext _context;

        public DeleteModel(IdolGroup.Data.IdolGroupContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Jobs Jobs { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Jobs = await _context.newsList
                .Include(j => j.NewsCat).FirstOrDefaultAsync(m => m.JobsID == id);

            if (Jobs == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Jobs = await _context.newsList.FindAsync(id);

            if (Jobs != null)
            {
                _context.newsList.Remove(Jobs);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
