using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using IdolGroup.Data;
using IdolGroup.Models;

namespace IdolGroup.Pages.NewsCategoryAdmin
{
    public class EditModel : PageModel
    {
        private readonly IdolGroup.Data.IdolGroupContext _context;

        public EditModel(IdolGroup.Data.IdolGroupContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Jobs Jobs { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Jobs = await _context.newsList
                .Include(j => j.NewsCat).FirstOrDefaultAsync(m => m.JobsID == id);

            if (Jobs == null)
            {
                return NotFound();
            }
           ViewData["JobsCategoryID"] = new SelectList(_context.NewsCategory, "JobsCategoryID", "JobsCategoryID");
            return Page();
        }

        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(Jobs).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!JobsExists(Jobs.JobsID))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool JobsExists(int id)
        {
            return _context.newsList.Any(e => e.JobsID == id);
        }
    }
}
